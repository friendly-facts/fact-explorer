Notes
=====

Performance of the eventFieldLens
---------------------------------

The eventFieldLens is an addition that helps user make sense of events faster.

To this end it will display information in a decrypted fashion.
If you do not have the ability to decrypt events enabled in the backend and
the data you are trying to display is encrypted, it will not work as expected.

Also if the events that contain your information are fairly complex or huge, you might
experience performance issues when using this lens.
