.. Fact Explorer documentation master file, created by
   sphinx-quickstart on Sun Dec  5 09:28:36 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root ``toctree`` directive.

Welcome to Fact Explorer
========================

A companion project to `factcast <https://github.com/factcast/factcast>`_ an event store written in Java.
You can also check out other event sourcing related project in python over at `pyfactcast <https://pypi.org/project/pyfactcast/>`_
as well as `cryptoshred <https://pypi.org/project/cryptoshred/>`_.


This project arose mainly out of the necessity to work with events directly instead
of through projections. Something that at the time of writing this is not supported by
the original factcast implementation. This is useful to:

- Enable searching for 'random' events quickly during debugging
- Checking events during migrations
- Very simple business checks for analysts to start thinking about their use cases
- Many other things (you should probably not do if you are an event sourcing purist)

Contributions are welcome. Just get in touch over on gitlab.

Be aware that this project is very minimal. No authentication. Not a lot of logging, almost no
error handling. It is an engineers tool. Use it at your own risk.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   quickstart
   developers/index
   frontend/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Sponsorships
------------

This project is sponsored by

.. image:: _static/prisma.png
   :target: https://www.prisma-capacity.eu/careers
