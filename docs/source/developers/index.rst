Developer Guide
===============

This project follows the *single_binary* approach to application development.
This means that you will always be able to simply run this app from the command line with
one command. You are however free to scale out separate components individually.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   setup
