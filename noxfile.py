from pathlib import Path
import nox
from nox import Session
from shutil import rmtree, copytree


@nox.session(python=["3.10", "3.8", "3.7"])
def tests(session: Session) -> None:
    args = session.posargs or ["--cov=fact_explorer"]
    session.run("poetry", "install", external=True)
    session.run("pytest", *args)


@nox.session
def docs(session: Session) -> None:
    session.run("poetry", "install", external=True)
    session.chdir("docs")
    session.run("rm", "-rf", "build/html", external=True)
    sphinx_args = ["-b", "html", "./source", "./build/html"]

    if "serve" in session.posargs:
        session.run(
            "sphinx-autobuild",
            "--port",
            "5000",
            "--open-browser",
            *sphinx_args,
            external=True
        )
    else:
        session.run("sphinx-build", *sphinx_args, external=True)


@nox.session(python=False)
def build_frontend(session: Session) -> None:
    rmtree(
        Path(__file__).parent.joinpath("src/fact_explorer/app/frontend/static/"),
        ignore_errors=True,
    )

    session.cd("src/fact_explorer/frontend")
    session.run("npm", "install")
    session.run("npm", "run", "build")


@nox.session(python=False)
def release(session: Session) -> None:
    build_for_dist_frontend(session=session)

    session.cd(Path(__file__).parent)

    session.run("poetry", "build")

    if "dry-run" in session.posargs:
        session.log("Would publish now but this is a dry-run")
    else:
        session.run("poetry", "publish")
        session.run("poetry", "version", "patch")
        session.log("Do not forget to set a proper version now.")

    if "keep" in session.posargs:
        session.log(
            (
                "Keeping distribution files for frontend."
                "This means your server will not start locally until you delete them."
            )
        )
    else:
        remove_fe_for_dist(session=session)


@nox.session(python=False)
def build_for_dist_frontend(session: Session) -> None:
    # TODO: Remove once https://github.com/python-poetry/poetry/issues/4583 is resolved
    build_frontend(session=session)

    rmtree(
        Path(__file__).parent.joinpath("fact_explorer"),
        ignore_errors=True,
    )

    session.cd(Path(__file__).parent)

    copytree(
        Path(__file__).parent.joinpath("src/fact_explorer/app/frontend/static"),
        Path(__file__).parent.joinpath("fact_explorer/app/frontend/static"),
    )

    Path(__file__).parent.joinpath("fact_explorer/__init__.py").touch()
    Path(__file__).parent.joinpath("fact_explorer/app/__init__.py").touch()
    Path(__file__).parent.joinpath("fact_explorer/app/frontend/__init__.py").touch()


@nox.session(python=False)
def remove_fe_for_dist(session: Session) -> None:
    rmtree(
        Path(__file__).parent.joinpath("fact_explorer"),
        ignore_errors=True,
    )
