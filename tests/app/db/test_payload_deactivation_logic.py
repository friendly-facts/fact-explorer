from fact_explorer.app.db.db_session import Database
from fact_explorer.config import Configuration


def test_pqs_allowed_no_pq_provided():
    db = Database(
        configuration=Configuration(
            database_password="fake", allow_payload_queries=True
        )
    )
    _, pq = db.determine_examples({}, None)

    assert pq == {}


def test_pqs_allowed_pq_provided():
    db = Database(
        configuration=Configuration(
            database_password="fake", allow_payload_queries=True
        )
    )
    _, pq = db.determine_examples({}, {"some": "query"})

    assert pq == {"some": "query"}


def test_pqs_not_allowed_no_pq_provided():
    db = Database(
        configuration=Configuration(
            database_password="fake", allow_payload_queries=False
        )
    )
    _, pq = db.determine_examples({}, None)

    assert pq == {}


def test_pqs_not_allowed_pq_provided():
    db = Database(
        configuration=Configuration(
            database_password="fake", allow_payload_queries=False
        )
    )
    _, pq = db.determine_examples({}, {"some": "query"})

    assert pq == {}


def test_header_replaced_if_none():
    db = Database(configuration=Configuration(database_password="fake"))

    hq, _ = db.determine_examples(None, {"some": "query"})

    assert hq == {}


def test_header_kept_if_exists():
    db = Database(configuration=Configuration(database_password="fake"))

    hq, _ = db.determine_examples({"some": "query_in_header"}, {"some": "query"})

    assert hq == {"some": "query_in_header"}
