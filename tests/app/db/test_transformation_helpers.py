from datetime import datetime
import pytest
from fact_explorer.app.db.db_session import _dict_converter, _extract_timestamp


@pytest.mark.asyncio
async def test_dict_converter():
    results = [{"header": '{"test":"header"}', "payload": '{"test":"payload"}'}]
    converted = await _dict_converter(results)

    assert converted == [{"header": {"test": "header"}, "payload": {"test": "payload"}}]


@pytest.mark.asyncio
@pytest.mark.skip(reason="Need to fix the timezone issues")
async def test_extract_timestamp():
    example = {"header": '{"meta":{"_ts":1644484804377}}'}
    res = await _extract_timestamp(example)

    assert res == datetime(2022, 2, 10, 10, 20, 4)
