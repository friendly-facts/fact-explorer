from imp import reload
from unittest.mock import AsyncMock
import pytest


@pytest.fixture(scope="function")
def db(mocker):
    from fact_explorer.app.db.db_session import Database

    patched = AsyncMock(Database)
    patched.fetch_by_time.return_value = [{"header": {}, "payload": {}}]
    patched.fetch_by_example.return_value = [
        {"header": {"by_example": True}, "payload": {}}
    ]

    return patched


@pytest.mark.asyncio
async def test_time_interval_can_be_used_with_decrypt(mock_decryptor, app_config, db):
    import fact_explorer.app.business.search as search
    import fact_explorer.app.business.crypto_helpers as ch

    reload(search)
    reload(ch)

    patched_function, _ = mock_decryptor

    patched_function.return_value = {}
    ch.find_and_decrypt = patched_function

    res = await search.time_interval(db=db, decrypt=True)

    assert res == [{"header": {}, "payload": {}}]
    patched_function.assert_called_once()


@pytest.mark.asyncio
async def test_time_interval_can_be_used_without_decrypt(
    mock_decryptor, app_config, db
):
    import fact_explorer.app.business.search as search
    import fact_explorer.app.business.crypto_helpers as ch

    reload(search)
    reload(ch)

    patched_function, _ = mock_decryptor

    patched_function.return_value = {"from": "mock"}
    ch.find_and_decrypt = patched_function

    res = await search.time_interval(db=db)

    assert res == [{"header": {}, "payload": {}}]
    patched_function.assert_not_called()


@pytest.mark.asyncio
async def test_strict_can_be_used_with_decrypt(mock_decryptor, app_config, db):
    import fact_explorer.app.business.search as search
    import fact_explorer.app.business.crypto_helpers as ch

    reload(search)
    reload(ch)

    patched_function, _ = mock_decryptor

    patched_function.return_value = {}
    ch.find_and_decrypt = patched_function

    res = await search.strict(db=db, decrypt=True)

    assert res == [{"header": {"by_example": True}, "payload": {}}]
    patched_function.assert_called_once()
    db.fetch_by_example.assert_called_once_with(
        offset=0, limit=20, header_example={}, payload_example={}
    )


@pytest.mark.asyncio
async def test_strict_can_be_used_without_decrypt(mock_decryptor, app_config, db):
    import fact_explorer.app.business.search as search
    import fact_explorer.app.business.crypto_helpers as ch

    reload(search)
    reload(ch)

    patched_function, _ = mock_decryptor

    patched_function.return_value = {}
    ch.find_and_decrypt = patched_function

    res = await search.strict(db=db)

    assert res == [{"header": {"by_example": True}, "payload": {}}]
    patched_function.assert_not_called()
    db.fetch_by_example.assert_called_once_with(
        offset=0, limit=20, header_example={}, payload_example={}
    )


@pytest.mark.asyncio
async def test_strict_takes_payload_queries(mock_decryptor, app_config, db):
    import fact_explorer.app.business.search as search
    import fact_explorer.app.business.crypto_helpers as ch

    reload(search)
    reload(ch)

    patched_function, _ = mock_decryptor

    patched_function.return_value = {}
    ch.find_and_decrypt = patched_function

    res = await search.strict(db=db, pq={"test": "example"})

    assert res == [{"header": {"by_example": True}, "payload": {}}]
    db.fetch_by_example.assert_called_once_with(
        offset=0, limit=20, header_example={}, payload_example={"test": "example"}
    )


@pytest.mark.asyncio
async def test_strict_takes_header_queries(mock_decryptor, app_config, db):
    import fact_explorer.app.business.search as search
    import fact_explorer.app.business.crypto_helpers as ch

    reload(search)
    reload(ch)

    patched_function, _ = mock_decryptor

    patched_function.return_value = {}
    ch.find_and_decrypt = patched_function

    res = await search.strict(db=db, hq={"test": "example"})

    assert res == [{"header": {"by_example": True}, "payload": {}}]
    db.fetch_by_example.assert_called_once_with(
        offset=0, limit=20, payload_example={}, header_example={"test": "example"}
    )
