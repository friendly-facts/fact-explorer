import logging

import pytest
import sys


@pytest.mark.asyncio
async def test_does_nothing_without_cryptoshred(mocker, caplog, app_config):
    if "cryptoshred.asynchronous.convenience" in sys.modules:
        del sys.modules["cryptoshred.asynchronous.convenience"]
    if "fact_explorer.app.business.crypto_helpers" in sys.modules:
        del sys.modules["fact_explorer.app.business.crypto_helpers"]

    mocker.patch.dict("sys.modules", {"cryptoshred": None})
    import fact_explorer.app.business.crypto_helpers as ch

    input_data = [{"some": "date"}]
    with caplog.at_level(logging.DEBUG):
        res = await ch.decrypt_result(data=input_data, key_backend=None)

    assert res == input_data
    assert "Cryptoshred not installed" in caplog.text


@pytest.mark.asyncio
async def test_decrypts_input(mock_decryptor, mocker, app_config):
    if "fact_explorer.app.business.crypto_helpers" in sys.modules:
        del sys.modules["fact_explorer.app.business.crypto_helpers"]

    if "fact_explorer.app.business.crypto_helpers.decrypt_result" in sys.modules:
        del sys.modules["fact_explorer.app.business.crypto_helpers.decrypt_result"]

    patched_function, _ = mock_decryptor
    patched_function.return_value = {}

    import fact_explorer.app.business.crypto_helpers as ch

    ch.find_and_decrypt = patched_function

    mocker.patch("fact_explorer.app.business.crypto_helpers.default_key_backend")

    # TODO Figure out why this does not use the mock under specific conditions
    # FIXME: pytest --cov-report html -s -v --cov-fail-under=85 --cov-branch  -vv --capture=no --randomly-seed=1640839930
    # fails if find_and_decrypt is not explicitly set.

    res = await ch.decrypt_result(data=[{"header": {}, "payload": {}}])

    assert res == [{"header": {}, "payload": {}}]
    patched_function.assert_called_once()


@pytest.mark.asyncio
async def test_returns_true_if_enabled(mock_decryptor, app_config):
    if "fact_explorer.app.business.crypto_helpers" in sys.modules:
        del sys.modules["fact_explorer.app.business.crypto_helpers"]

    if "fact_explorer.app.business.crypto_helpers.decrypt_result" in sys.modules:
        del sys.modules["fact_explorer.app.business.crypto_helpers.decrypt_result"]

    import fact_explorer.app.business.crypto_helpers as ch

    res = await ch.cryptoshred_enabled()

    assert res


@pytest.mark.asyncio
async def test_returns_false_if_not_enabled(mocker, caplog, app_config):
    if "cryptoshred.asynchronous.convenience" in sys.modules:
        del sys.modules["cryptoshred.asynchronous.convenience"]
    if "fact_explorer.app.business.crypto_helpers" in sys.modules:
        del sys.modules["fact_explorer.app.business.crypto_helpers"]

    mocker.patch.dict("sys.modules", {"cryptoshred": None})
    import fact_explorer.app.business.crypto_helpers as ch

    res = await ch.cryptoshred_enabled()

    assert not res
