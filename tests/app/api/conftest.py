import pytest
from shutil import rmtree
from pathlib import Path


@pytest.fixture(scope="session")
def fe_dir():
    app_root = Path(__file__).parent.parent.parent.parent
    static_path = app_root.joinpath("src/fact_explorer/app/frontend/static/")
    clean_up = True

    try:
        static_path.mkdir()
    except FileExistsError:
        clean_up = False

    yield

    if clean_up:
        rmtree(
            static_path,
            ignore_errors=True,
        )
