from typing import Optional, Dict, Union, Any

API_RESPONSES: Optional[Dict[Union[int, str], Dict[str, Any]]] = {
    404: {"description": "Not Found!"}
}
