// small word of warning - not the most beautiful code ever, sorry
// first goals is to get everything working
import { iterator, parse } from '@humanwhocodes/momoa';
import { Monaco } from '@monaco-editor/react';
import * as queryString from 'query-string';
import { limitFetch } from './limit-fetch';

type Item = { node: any; id: string; config: EventFieldLensConfig };

let lastLensCacheKey = '';
let chachedItems: Item[] = [];
const valueCache: Record<string, string> = {};

export function eventFieldLens(
  monaco: Monaco,
  configs: EventFieldLensConfig[]
) {
  monaco.languages.registerCodeLensProvider('json', {
    async provideCodeLenses(model, token) {
      if (!model.uri.path.startsWith('/result/')) return;

      // get items (from cache if possible)
      const cacheKey = `${model.id}-${model.getVersionId()}`;
      if (cacheKey !== lastLensCacheKey) {
        const ast = parse(model.getValue());
        const items: Item[] = [];

        for (const { node, parent, phase } of iterator(
          ast,
          ({ phase }: any) => phase === 'enter'
        )) {
          configs.forEach((config) => {
            if (
              node.type === 'String' &&
              config.fields.some((field) =>
                typeof field === 'string'
                  ? field === node.value
                  : node.value.match(field)
              ) &&
              parent.type === 'Member' &&
              parent.value.type === 'String'
            ) {
              const id = parent.value.value;
              items.push({ node, id, config });
            }
          });
        }

        lastLensCacheKey = cacheKey;
        chachedItems = items;
      }

      const lenses = (
        await Promise.all(
          chachedItems.map(async (item) => {
            const title = await getValue(item.id, item.config);

            if (!title) return null;

            return {
              range: new monaco.Range(
                item.node.loc.start.line,
                item.node.loc.start.column,
                item.node.loc.end.line,
                item.node.loc.end.column
              ),
              command: {
                id: '',
                title: title,
              },
            };
          })
        )
      ).filter(<Value>(value: Value | null): value is Value => !!value);

      return {
        lenses,
        dispose: () => {},
      };
    },
  });
}

async function getValue(id: string, config: EventFieldLensConfig) {
  // use cache if possible
  if (valueCache[id]) return valueCache[id];

  for (const eventMatcher of config.events) {
    const { value, ...query } = eventMatcher;

    const params = queryString.stringify({
      hq: JSON.stringify({ ...query, aggIds: [id] }),
      pq: JSON.stringify({}),
      skip: 0,
      limit: 1,
      decrypt: true,
    });
    const url = `/api/search?${params}`;
    const response = await limitFetch(url, () => fetch(url));

    if (!response.ok) throw await response.clone().json();

    const data = await response.clone().json();
    if (data.length) {
      const value = eventMatcher.value(data[0].payload);
      valueCache[id] = value;
      return value;
    }
  }

  return null;
}
