import { ParsedUrlQuery } from 'querystring';

export function isSameQuery(
  query: ParsedUrlQuery,
  values: Record<string, unknown>
) {
  for (const key in values) {
    const value = values[key];
    const same =
      typeof value === 'string'
        ? value === query[key]
        : String(value) === query[key];
    if (!same) return false;
  }
  return true;
}
