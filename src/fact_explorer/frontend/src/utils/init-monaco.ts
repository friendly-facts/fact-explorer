// small word of warning - not the most beautiful code ever, sorry
// first goals is to get everything working
import { loader } from '@monaco-editor/react';
import { actionsForAggId } from './actions-for-agg-id';
import { codeCompletionForHeader } from './code-completion-for-header';
import { documentationForPayload } from './documentation-for-payload';
import { eventFieldLens } from './event-field-lens';
import { humanReadableTimestampForHeader } from './human-readable-timestamp-for-header';

loader.init().then(async (monaco) => {
  if (!window.FACT_EXPLORER.disableCodeCompletionForHeader)
    codeCompletionForHeader(monaco);

  humanReadableTimestampForHeader(monaco);

  actionsForAggId(monaco);

  if (!window.FACT_EXPLORER.disableDocumentationForPayload)
    documentationForPayload(monaco);

  if (window.FACT_EXPLORER.eventFieldLens)
    eventFieldLens(monaco, window.FACT_EXPLORER.eventFieldLens);
});
