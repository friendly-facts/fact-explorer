const MAX_CONCURRENCY = 10;
const cache: Record<string, Promise<Response> | undefined> = {};

const pendingCallbacks: Array<() => Promise<Response>> = [];
const waitingCallbacks: Array<() => Promise<Response>> = [];

function handleWaitingCallbacks(callback: () => Promise<Response>) {
  const index = pendingCallbacks.findIndex((c) => c === callback);
  pendingCallbacks.splice(index, 1);
  const waiting = waitingCallbacks.shift();
  if (waiting) {
    pendingCallbacks.push(waiting);
    waiting().then(() => {
      handleWaitingCallbacks(waiting);
    });
  }
}

// in order to avoid stress on the backend we
// limit max. concurrency low-prio fetches and try to cache responses to the same route
export function limitFetch(
  key: string,
  callback: () => Promise<Response>
): Promise<Response> {
  const chachedPromise = cache[key];
  if (chachedPromise) return chachedPromise;

  if (pendingCallbacks.length < MAX_CONCURRENCY) {
    pendingCallbacks.push(callback);
    const promise = callback().then((response) => {
      handleWaitingCallbacks(callback);
      return response;
    });
    cache[key] = promise;
    return promise;
  } else {
    let _resolve: (response: Response) => void;
    const promise = new Promise<Response>((resolve) => (_resolve = resolve));
    waitingCallbacks.push(() =>
      callback().then((response) => {
        _resolve(response);
        return response;
      })
    );
    cache[key] = promise;
    return promise;
  }
}
