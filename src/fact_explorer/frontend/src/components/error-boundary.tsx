import Head from 'next/head';
import { useRouter } from 'next/router';
import {
  Component,
  ErrorInfo,
  FC,
  ReactNode,
  useEffect,
  useState,
} from 'react';
import { FetchError } from '../hooks/use-fetch';
import { Card } from './card';
import { Title } from './title';

type RealErrorBoundaryProps = {
  children?: ReactNode;
  path: string;
  setErrorPath: (value: string | null) => void;
};

type State = { error: unknown };

class RealErrorBoundary extends Component<RealErrorBoundaryProps, State> {
  state: State = { error: undefined };

  static getDerivedStateFromError(error: unknown) {
    return { error };
  }

  componentDidCatch(error: unknown, errorInfo: ErrorInfo) {
    this.props.setErrorPath(this.props.path);
  }

  render() {
    const { error } = this.state;

    if (error === undefined) return this.props.children;
    console.dir(error);
    return (
      <>
        <Head>
          <title>Error</title>
        </Head>

        <Title>Error</Title>

        <Card>
          <div className="p-5">
            {error instanceof FetchError ? (
              <>
                <p>We&apos;re sorry. There was a problem with the request.</p>
                <p>
                  <small>
                    Received {error.res.status} for {error.init.method || 'GET'}{' '}
                    {error.info}.
                  </small>
                </p>
              </>
            ) : (
              <p>We&apos;re sorry. An unknown error happened.</p>
            )}
          </div>
        </Card>
      </>
    );
  }
}

export const ErrorBoundary: FC = ({ children }) => {
  const [key, setKey] = useState(0);
  const { asPath } = useRouter();
  const [errorPath, setErrorPath] = useState<string | null>(null);

  useEffect(() => {
    if (errorPath !== null) {
      // unmount the error boundary on location change after an error happened to reset error state
      if (errorPath !== asPath) {
        setErrorPath(null);
        setKey((key) => key + 1);
      }
    }
  }, [asPath, errorPath]);

  return (
    <RealErrorBoundary key={key} path={asPath} setErrorPath={setErrorPath}>
      {children}
    </RealErrorBoundary>
  );
};
