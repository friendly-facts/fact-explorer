import Editor from '@monaco-editor/react';
import type { editor } from 'monaco-editor/esm/vs/editor/editor.api';
import { useEffect, useMemo, useState, VFC, memo } from 'react';
import { FetchRequest } from '../hooks/use-fetch';
import { FactOut } from '../types/types';
import get from 'lodash/get';
import uniq from 'lodash/uniq';
import { useRouter } from 'next/router';
import { useParams } from '../hooks/use-params';
const JMeta = require('jmeta');

type FoldLevel = '1' | '2' | '3' | '4' | '5' | '6' | '7' | 'unfold';

type ResultSettings = {
  tableView: boolean;
  foldLevel: FoldLevel;
  columns: string[];
};

const defaultParams: ResultSettings = {
  tableView: false,
  foldLevel: '4',
  columns: ['header.meta._ser', 'header.ns', 'header.type'],
};

export const Result: VFC<{
  request: FetchRequest<FactOut[]>;
  path?: string;
}> = ({ request, path }) => {
  const params = useParams(defaultParams);
  const router = useRouter();

  const [editor, setEditor] = useState<editor.IStandaloneCodeEditor | null>(
    null
  );

  const [tableView, setTableView] = useState(params.tableView);
  const [foldLevel, setFoldLevel] = useState(params.foldLevel);
  const [columns, setColumns] = useState(params.columns);

  const paths = useMemo(() => {
    if (!request.data) return [];

    const paths = request.data.map((item) => new JMeta(item).paths()).flat();
    return uniq(paths.concat(params.columns)).sort();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [request.data]);

  useEffect(() => {
    if (!editor) return;
    editor.trigger('unfold', 'editor.unfoldAll', {});
    if (foldLevel === 'unfold') return;

    if (foldLevel === '1') editor.trigger('fold', 'editor.foldAll', {});
    else editor.trigger('fold', `editor.foldLevel${foldLevel}`, {});
  }, [foldLevel, editor]);

  return (
    <>
      <div className="p-4 flex gap-5">
        <label className="text-xs flex gap-1 items-start">
          <span>Table View:</span>
          <div>
            <input
              type="checkbox"
              className="h-5 w-5 focus:ring-blue-500 focus:border-blue-500 shadow-sm border border-gray-300 rounded-md appearance-none checked:bg-blue-500 checked:after:content-['✓'] checked:after:grid checked:after:text-white checked:after:text-center"
              onChange={(e) => {
                setTableView(!tableView);
                router.replace(
                  {
                    query: { ...router.query, tableView: !tableView },
                  },
                  undefined,
                  { scroll: false }
                );
              }}
              checked={tableView}
            />
          </div>
        </label>

        {!tableView && (
          <label className="text-xs flex gap-1 items-start">
            <span>Folding:</span>
            <select
              className="focus:ring-blue-500 focus:border-blue-500 shadow-sm border border-gray-300 rounded-md p-0.5 text-xs disabled:bg-gray-200 disabled:cursor-not-allowed"
              onChange={(e) => {
                const foldLevel = e.currentTarget.value;
                setFoldLevel(foldLevel as FoldLevel);
                router.replace(
                  {
                    query: { ...router.query, foldLevel },
                  },
                  undefined,
                  { scroll: false }
                );
              }}
              value={foldLevel}
            >
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
              <option value="6">6</option>
              <option value="7">7</option>
              <option value="unfold">Unfold All</option>
            </select>
          </label>
        )}

        {tableView && (
          <label className="text-xs flex gap-1 items-start">
            <span>Columns:</span>
            <select
              className="w-100 focus:ring-blue-500 focus:border-blue-500 shadow-sm border border-gray-300 rounded-md p-0.5 text-xs disabled:bg-gray-200 disabled:cursor-not-allowed"
              onChange={(e) => {
                const column = e.currentTarget.value;
                const foundIndex = columns.findIndex((c) => c === column);
                const newColumns =
                  foundIndex > -1
                    ? [
                        ...columns.slice(0, foundIndex),
                        ...columns.slice(foundIndex + 1),
                      ]
                    : [...columns, column].sort();
                setColumns(newColumns);
                router.replace(
                  {
                    query: { ...router.query, columns: newColumns },
                  },
                  undefined,
                  { scroll: false }
                );
              }}
              value={columns}
              multiple
              disabled={!request.data}
            >
              {request.data ? (
                paths.map((path) => (
                  <option key={path} value={path} className="p-1">
                    {path}
                  </option>
                ))
              ) : (
                <option>Loading...</option>
              )}
            </select>
          </label>
        )}
      </div>

      {request.loading ? (
        <div className="p-5 shadow-inner">
          <span className="inline-block animate-spin">↻</span> Loading...
        </div>
      ) : request.data ? (
        <ShowData
          data={request.data}
          path={path}
          setEditor={setEditor}
          tableView={tableView}
          columns={columns}
        />
      ) : null}
    </>
  );
};

const ShowData: VFC<{
  data: FactOut[];
  path?: string;
  setEditor: (editor: editor.IStandaloneCodeEditor | null) => void;
  tableView: boolean;
  columns: string[];
}> = ({ data, path, setEditor, tableView, columns }) => {
  const value = useMemo(() => {
    if (!data.length) return null;
    return JSON.stringify(data, null, 2);
  }, [data]);

  if (!value)
    return (
      <div className="p-5 shadow-inner">
        <p>No result.</p>
      </div>
    );

  return (
    <>
      {tableView ? (
        <Table data={data} columns={columns} />
      ) : (
        <Editor
          path={path}
          height={600}
          defaultLanguage="json"
          value={value}
          options={{
            scrollBeyondLastLine: false,
            readOnly: true,
            minimap: { enabled: false },
            unusualLineTerminators: 'auto',
          }}
          onMount={(editor, monaco) => {
            setEditor(editor);

            // hide read-only tooltip
            // see https://github.com/microsoft/monaco-editor/issues/1742#issuecomment-998853901
            const messageContribution = editor.getContribution(
              'editor.contrib.messageController'
            );
            editor.onDidAttemptReadOnlyEdit(() =>
              messageContribution.dispose()
            );
          }}
        />
      )}
    </>
  );
};

const Table: VFC<{ data: FactOut[]; columns: string[] }> = memo(
  ({ data, columns }) => {
    return (
      <table className="w-full text-gray-700" style={{ tableLayout: 'fixed' }}>
        <thead>
          <tr className="text-xs text-left bg-gray-200">
            {columns.map((column) => (
              <th className="py-1 px-3" key={column}>
                {column}
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {data.map((item, index) => (
            <TableRow
              key={item.header.id}
              data={item}
              index={index}
              columns={columns}
            />
          ))}
        </tbody>
      </table>
    );
  }
);
Table.displayName = 'Table';

const TableRow: VFC<{ data: FactOut; index: number; columns: string[] }> = ({
  data,
  index,
  columns,
}) => {
  return (
    <tr className={`text-sm ${index % 2 ? 'bg-gray-100' : ''} align-top`}>
      {columns.map((column) => {
        const value = get(data, column);
        return (
          <td key={column} className="py-1 px-3">
            {value === undefined ? (
              <em>empty</em>
            ) : value === null ? (
              <code>null</code>
            ) : typeof value === 'object' ? (
              <pre style={{ whiteSpace: 'pre-wrap' }}>
                <code>{JSON.stringify(value, null, 2)}</code>
              </pre>
            ) : (
              value
            )}
          </td>
        );
      })}
    </tr>
  );
};
