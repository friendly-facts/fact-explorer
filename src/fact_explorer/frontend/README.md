This is the frontend for the Fact Explorer.

## Prequisites

Please have a recent version of [Node](https://nodejs.org) installed.

## Development Instructions

Run `$ npm install` for the initial setup (or whenever dependencies change).

Run `$ npm run dev` to start the local development server which will be available on [localhost](http://localhost:3000/).

You can run `$ npm run lint` for some basic linter checks and `$ npm run format` to automatically apply formatting.

## Usage Instructions

You can change the runtime behaviour of the application by overwriting the `config.js` during deployment. By default it just looks like this:

```js
window.FACT_EXPLORER = {};
```

### config: `disableCodeCompletionForHeader`

If set to `true` you'll not see code completion in the header query input field.

```js
window.FACT_EXPLORER = {
  disableCodeCompletionForHeader: true,
};
```

### config: `disableDocumentationForPayload`

If set to `true` you'll not see documentation on hover on the results.

```js
window.FACT_EXPLORER = {
  disableDocumentationForPayload: true,
};
```

### config: `eventFieldLens`

The `eventFieldLens` allows you to match any event field and provide a code lens based on some event data. The matched event field **needs** to refer to an id which can be used in the `aggIds` header. In the easiest case it could look like this:

```js
window.FACT_EXPLORER = {
  eventFieldLens: [
    {
      fields: ['userId'],
      events: [
        {
          type: 'UserData',
          value: (eventData) => `${eventData.firstName} ${eventData.lastName}`,
        },
      ],
    },
  ],
};
```

Whenever a `userId` field can be found a `UserData` event is fetched and `${eventData.firstName} ${eventData.lastName}` will be shown as a code lens above the user id.

You can also use RegExp's or fallback events.

```js
window.FACT_EXPLORER = {
  eventFieldLens: [
    {
      fields: ['userId', /UserId$/],
      events: [
        {
          type: 'UserChanged',
          value: (eventData) => `${eventData.firstName} ${eventData.lastName}`,
        },
        {
          type: 'UserCreated',
          ns: 'organisation',
          version: 1,
          value: (eventData) => `${eventData.firstName} ${eventData.lastName}`,
        },
      ],
    },
  ],
};
```

In this case the `UserChanged` event is fetched and when it doesn't exist yet for this user id the `UserCreated` event for this user id will be fetched.
You can also specify the `ns` and `version`.
